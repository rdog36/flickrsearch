class CreateSearches < ActiveRecord::Migration[5.0]
  def change
    create_table :searches do |t|
      t.string :tags
      t.date :min_upload_date
      t.date :max_upload_date
      t.string :sort
      t.boolean :is_getty
      t.timestamps
    end
  end
end
