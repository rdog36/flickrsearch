class AddUniqueIndexToSearches < ActiveRecord::Migration[5.0]
  def change
  	add_index :searches, [:tags, :min_upload_date, :max_upload_date, :is_getty, :sort], unique: true, name: "search_uniqueness_index"
  end
end
