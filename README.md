Flickrsearch 

# What?

	This is a fun little exercise in the finer points of Rails API and DB implementation. It allows one to use REST calls to Flickr's Photo Search API to search Flickr's photography database. It returns the search results as JSON and renders them to the browser. It is also designed to save the input set of search parameters to the database, but leverages uniqueness validation to only save the first instance of a specific search. The search will proceed and results will be displayed whether or not the search parameters are saved to the database.

# How? 

	Simply fire up the application, and add any subset of the following parameters (or don't, as they have default values if unassigned):

		tags: Search tags that usually correlate directly with the content of the photograph. Accepts multiple tags, comma-separated.  

		  Default value: "bird"

		min_upload_date: Bottom of date range for search. 

		  Default value: "1/1/2021"

		max_upload_date: Top of date range for search. Default value: "1/13/2021"

		is_getty: Flag to determine whether the photo is licensed as stock in the Getty Images collection. Accepts: 
		  true, 
		  false, 
		  nil (to disregard flag) 

		  Default value: nil 

		sort: Chooses the style of sort from the search results. Accepts: 
		  date-posted-asc, 
		  date-posted-desc, 
		  date-taken-asc, 
		  date-taken-desc, 
		  interestingness-desc, 
		  interestingness-asc, 
		  relevance. 

		  Default value: "relevance"


	example call, hosted locally:  https://localhost:3000?tags="fire hydrant"&min_upload_date="12/20/2020"&max_upload_date="1/11/2021"&is_getty="true"&sort=date-posted-desc"

	For more information on the Flickr Photo Search API and its options, please see the official documentation: https://www.flickr.com/services/api/flickr.photos.search.html