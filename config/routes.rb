Rails.application.routes.draw do
  
  get 'search/index'
  root 'search#index'
  resource :search, only: [:index]

end
