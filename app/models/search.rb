require 'httparty'
require 'pry'

class Search < ApplicationRecord
  include HTTParty

  validates :tags , uniqueness: { scope: [:min_upload_date, :max_upload_date, :is_getty, :sort] }

  ENV["FLICKR_API_KEY"] = "12a39b460b5c1326295a2169814e7430"
  base_uri "https://www.flickr.com"

  def self.for(options = {})
    return get('/services/rest/', :query => {
	  :method => 'flickr.photos.search',
	  :api_key => ENV["FLICKR_API_KEY"],
	  :format => 'json',
	  :nojsoncallback => 1,
	  :tags => options[:tags],
	  :min_upload_date => options[:min_upload_date],
	  :max_upload_date => options[:max_upload_date],
	  :is_getty => options[:is_getty],
	  :sort => options[:sort] })
	end
 end
