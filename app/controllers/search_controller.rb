class SearchController < ApplicationController
  require 'pry'

  def index
    @tags = params[:tags] || "bird"
	@min_upload_date = params[:min_upload_date] || "1/1/2021"
	@max_upload_date = params[:max_upload_date] || "1/13/2021"
	@is_getty = params[:is_getty] || nil
    @sort = params[:sort] || "relevance"
	@results = Search.for(tags: @tags, min_upload_date: @min_upload_date, max_upload_date: @max_upload_date, is_getty: @is_getty, sort: @sort)
	search = Search.new(
	  tags: @tags , 
      min_upload_date: @min_upload_date , 
	  max_upload_date: @max_upload_date ,
	  is_getty: @is_getty ,
	  sort: @sort
	)
	begin
	  search_save_success = search.save
 	rescue
 	  puts "An exception was thrown when saving to the database, but we continue anyway"
 	end
	  search_save_message = search_save_success ? "The search was successfully saved to the database!" : "This particular search has already been peformed, so it will not be saved to the database."
	  render json: { message: search_save_message, search_results: @results }
	end
end 
