require 'rails_helper'

RSpec.describe 'Searches API', type: :request do

  describe 'GET /search' do

  	before { get '/' } 

  	it 'returns the searches' do
  	  expect(json).not_to be_empty
      expect(json.keys[0]).to eq("message")
      expect(json.keys[1]).to eq("search_results")
  	end

  	it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

  end 
end
