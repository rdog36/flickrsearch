require 'rails_helper'

RSpec.describe Search, type: :model do
  let(:custom_search_attributes) {
  								   { 
  								   	 tags: "burritos", 
  								     min_upload_date: "1/3/2021", 
  								     max_upload_date: "1/17/2021",
  								     is_getty: true,
  								     sort: "date-taken-asc" 
  								   }
  								  }  

  it "the default search without parameters is valid" do
    expect(Search.new()).to be_valid
  end

  it "validates and saves new unique searches with supplied parameters" do
  	new_search = Search.new(custom_search_attributes)
  	expect(new_search.save).to be true
  end

  it "rejects existing searches" do
  	new_search_1 = Search.new(custom_search_attributes)
  	new_search_2 = Search.new(custom_search_attributes)
  	expect(new_search_1.save).to be true
  	expect(new_search_2.save).to be false
  end

end
