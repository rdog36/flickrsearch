# spec/factories/searches.rb
FactoryBot.define do
  factory :search do
    tags { Faker::Lorem.word }
    min_upload_date { Faker::Date.between(40.days.ago, 15.days.ago) }
    max_upload_date { Faker::Date.between(14.days.ago, Date.today )}
    is_getty { [true, false, nil].sample }
    sort { 
    		['date-posted-asc' , 
		    'date-posted-desc' , 
		    'date-taken-asc' , 
		    'date-taken-desc' , 
		    'interestingness-desc' , 
		    'interestingness-asc' , 
		    'relevance'].sample 
		 }
  end
end